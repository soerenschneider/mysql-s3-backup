FROM mysql:5.7.33

RUN apt update \
    && apt -y install --no-install-recommends gnupg2 awscli \
    && apt -y clean \
    && useradd -ms /bin/bash mysql_s3_backup

COPY mysql_s3_backup /usr/bin
USER mysql_s3_backup
WORKDIR /home/mysql_s3_backup
ENTRYPOINT ["/usr/bin/mysql_s3_backup"]
